package id.co.nexsoft.formative163;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Transactional
@RestController
public class WebController {
	@Autowired
	SendingAddressRepository repo;

	@GetMapping("/allData")
	public List<SendingAddress> listData() {
		return (List<SendingAddress>) repo.findAll();
	}

	@GetMapping("/data/{id}")
	public SendingAddress getDataById(@PathVariable(value = "id") int id) {
		return repo.findById(id);
	}

	@GetMapping(value = "/data/delete/{id}")
	public void deleteData(@PathVariable(value = "id") int id) {
		repo.deleteById(id);
	}

	@PostMapping("data/update")
	public void updateDataZipCodeByRecipient(@RequestBody SendingAddress data) {
		performUpdateZipCodeByRecipient(repo, data.getZipCode());
	}

	private void performUpdateZipCodeByRecipient(SendingAddressRepository taskRepository, String zip) {
		SendingAddress data = taskRepository.findByRecipient("Daniel Bram");

		if (data != null) {
			data.setZipCode(zip);
			taskRepository.save(data);
		}
	}
}
