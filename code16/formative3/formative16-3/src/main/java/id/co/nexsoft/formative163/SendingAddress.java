package id.co.nexsoft.formative163;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

@Entity
public class SendingAddress {
	@Id
	int id;
	String recipient;
	String gender;
	String street;
	String zipCode;
	String district;
	String city;
	String province;
	String country;
	public SendingAddress() {
		
	}	
	public SendingAddress(int id, String recipient, String gender, String street, String zipCode, String district,
			String city, String province, String country) {
		this.id = id;
		this.recipient = recipient;
		this.gender = gender;
		this.street = street;
		this.zipCode = zipCode;
		this.district = district;
		this.city = city;
		this.province = province;
		this.country = country;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
}
