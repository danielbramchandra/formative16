package id.co.nexsoft.formative163;

import java.util.List;

import org.springframework.data.repository.CrudRepository;


public interface SendingAddressRepository extends CrudRepository<SendingAddress, Integer>{
	SendingAddress findById(int id);
	SendingAddress findByRecipient(String name);
	List<SendingAddress> findAll();
	void deleteById(int id);
}
