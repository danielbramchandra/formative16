package id.co.nexsoft.formative163;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative163Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative163Application.class, args);
	}

}
