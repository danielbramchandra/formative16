package id.co.nexsoft.formative16;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
@ComponentScan({ "id.co.nexsoft.formative16" })
public class MVCconfig extends WebMvcConfigurerAdapter{

}
