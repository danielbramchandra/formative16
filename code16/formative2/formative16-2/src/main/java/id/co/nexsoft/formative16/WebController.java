package id.co.nexsoft.formative16;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.mysql.cj.jdbc.ConnectionImpl;
import com.mysql.cj.jdbc.MysqlDataSource;
import com.mysql.cj.jdbc.PreparedStatementWrapper;
import com.mysql.cj.jdbc.StatementImpl;

@Controller
@EnableWebMvc
public class WebController {

	@RequestMapping("/add")
	public ModelAndView showview(SendingAddress data) throws SQLException {
		String url = "jdbc:mysql://localhost:3306/formative16";
		MysqlDataSource dataSource = new MysqlDataSource();
		dataSource.setURL(url);
		dataSource.setUser("root");
		dataSource.setPassword("90215121");
		try (ConnectionImpl conn = (ConnectionImpl) dataSource.getConnection()) {
			String query = "INSERT INTO sendingAddress(recipient,gender,street,zipCode,district,city,province,country,assurance)VALUES(?,?,?,?,?,?,?,?,?)";
			try (PreparedStatement statement = conn.prepareStatement(query)) {
				statement.setString(1, data.getRecipient());
				statement.setString(2, data.getGender());
				statement.setString(3, data.getStreet());
				statement.setString(4, data.getZipCode());
				statement.setString(5, data.getDistrict());
				statement.setString(6, data.getCity());
				statement.setString(7, data.getProvince());
				statement.setString(8, data.getCountry());
				statement.setBoolean(9, data.isAssurance());
				statement.executeUpdate();

			}
			

			ModelAndView mv = new ModelAndView();
			mv.setViewName("index.jsp");
			return mv;
		}
	}
}
